#include<stdio.h>
struct frac
{
    float a;
    float b;
};

void input(struct frac A[],int n)
{
    int i;
    for (i=0;i<n;i++)
    {
        printf("Enter the Numerator\n");
        scanf("%f",&A->a);
        printf("Enter the Denominator\n");
        scanf("%f",&A->b);
    }
}

void output(struct frac c)
{
    if(c.a!=c.b)
        printf("The sum of the fractions is %f/%f\n",c.a,c.b);
    else
        printf("The sum of the fractions is %f\n",c.a);
}

struct frac compute(struct frac A[],int n)
{
    struct frac temp,C;
    temp.a=temp.b=0;
    int i,a,b,t,gcd;
    for(i=0;i<n;i++)
    {
        temp.a=temp.a+(A[i].a/A[i].b);
    }
    temp.a=temp.a*10;
    temp.b=10;
    C.a=temp.a;
    C.b=temp.b;
    a=C.a;
    b=C.b;
    while (b != 0)
    {
        t = b;
        b = a%b;
        a = t;
    }
    gcd=a;
    C.a=C.a/(gcd*10);
    C.b=C.b/(gcd*10);
    return C;
}
int main()
{
    struct frac x[20],z;
    int n;
    printf("Enter the number of entries\n");
    scanf("%d",&n);
    input(&x[20],n);
    z=compute(&x[20],n);
    output(z);
    return 0;
}
