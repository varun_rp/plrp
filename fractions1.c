#include<stdio.h>
struct frac
{
    int a;
    int b;
};

void input(struct frac *A)
{
    printf("Enter the Fraction\n");
    scanf("%d/%d",&A->a,&A->b);
}

struct frac compute(struct frac A,struct frac B)
{
    struct frac C;
    int a,b,t,gcd;
    C.a=(A.a*B.b + A.b*B.a);
    C.b=(A.b*B.b);
    a=C.a;
    b=C.b;
    while (b != 0)
    {
        t = b;
        b = a%b;
        a = t;
    }
    gcd=a;
    C.a=C.a/gcd;
    C.b=C.b/gcd;
    return C;
    /*int d;
    d=C.a/C.b;
    if(C.a>C.b)
    {
        if(d>0)
        {
            C.a=C.a/d;
            C.b=C.b/d;
        }
        return C;
    }
    else
        return C;
    */
}

void output(struct frac a,struct frac b,struct frac c)
{
    if(c.a!=c.b)
        printf("The sum of the fractions %d/%d and %d/%d is %d/%d\n",a.a,a.b,b.a,b.b,c.a,c.b);
    else
        printf("The sum of the fractions %d/%d and %d/%d is %d\n",a.a,a.b,b.a,b.b,c.a);
}

int main()
{
    struct frac x,y,z;
    input(&x);
    input(&y);
    z=compute(x,y);
    output(x,y,z);
    return 0;
}
