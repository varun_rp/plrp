#include<stdio.h>
struct frac
{
    int num;
    int deno;
};

void input(struct frac A[],int n)
{
    int i;
    for (i=0;i<n;i++)
    {
        printf("Enter the Fraction in the form a/b. P.s add '/'\n");
        scanf("%d/%d",&A[i].num,&A[i].deno);
    }
}

int entries()
{
    int n;
    printf("Enter the number of entries\n");
    scanf("%d",&n);
    return n;
}
void output(struct frac c)
{
    if(c.num!=c.deno && c.deno!=1)
        printf("The sum of the fractions is %d/%d\n",c.num,c.deno);
    else
        printf("The sum of the fractions is %d\n",c.num);
}

int hcf(int a,int b)
{
    int t,gcd;
    while (b != 0)
    {
        t = b;
        b = a%b;
        a = t;
    }
    gcd=a;
    return gcd;    
}
struct frac compute(struct frac A[],int n)
{
    struct frac C;
    int a,b,t,gcd;
    if(n>1)
    {
        C.num=(A[0].num*A[1].deno + A[0].deno*A[1].num);
        C.deno=(A[0].deno*A[1].deno);
        for(int i=2;i<n;i++)
        {
            C.num=(C.num*A[i].deno + C.deno*A[i].num);
            C.deno=C.deno*A[i].deno;
        }
        a=C.num;
        b=C.deno;
        
        gcd=hcf(a,b);
        C.num=C.num/gcd;
        C.deno=C.deno/gcd;
    }
    else 
    {
        C.num=A[0].num;
        C.deno=A[0].deno;
    }
    return C;
}
int main()
{
    struct frac z;
    int n;
    n=entries();
    struct frac x[n];
    input(x,n);
    z=compute(x,n);
    output(z);
    return 0;
}