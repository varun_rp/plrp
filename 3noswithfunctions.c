#include<stdio.h>

void input(float *a,float *b,float *c)
{
    printf("Enter the First Number\n");
    scanf("%f",a);
    printf("Enter the Second Number\n");
    scanf("%f",b);
    printf("Enter the Third Number\n");
    scanf("%f",c);
}

float compute(float a,float b,float c)
{
    if(a>=b&&a>=c)
        return a;
    else if(b>=c)
        return b;
    else 
        return c;
}

void output(float a,float b,float c,float d)
{
    printf("The Greatest number between %f , %f and %f is : %f\n",a,b,c,d);
}

int main()
{
    float a,b,c,d;
    input(&a,&b,&c);
    d=compute(a,b,c);
    output(a,b,c,d);
    return 0;
}