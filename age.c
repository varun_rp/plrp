#include<stdio.h>

void input(float *a)
{
    printf("Enter the Age\n");
    scanf("%f",a);
}

int compute(float a)
{
    if(a>=18)
        return 1;
    else 
        return 0;
}

void output(int a)
{
    if(a==1)
        printf("The Candidate is Eligible to Vote\n");
    else if(a==0)
        printf("The Candidate is not Eligible to Vote\n");
}

int main()
{
    float a;
    int b;
    input(&a);
    b=compute(a);
    output(b);
    return 0;
}