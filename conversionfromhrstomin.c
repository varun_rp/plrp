#include<stdio.h>

int convert(int hours,int mins)
{
    return 60*hours+mins;
}

void input(int *a,int *b)
{   
    printf("Enter the hours\n");
    scanf("%d",a);
    printf("Enter the minutes\n");
    scanf("%d",b);
}

void output(int *a)
{
    printf("The total minutes is %d\n",*a);
}

int main()
{
    int hours,mins,total;
    input(&hours,&mins);
    total=convert(hours,mins);
    output(&total);
    return 0;
}