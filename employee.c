#include<stdio.h>

struct date
{
    int day;
    char month[15];
    int year;
};

struct employee
{
    int employee_id;
    char name[50];
    float salary;
    struct date doj;
};

void input(struct employee *a)
{
    printf("Enter Employee ID\n");
    scanf("%d",&a->employee_id);
    printf("Enter Employee Name\n");
    scanf("%s",&a->name);
    printf("Enter Employee Salary\n");
    scanf("%f",&a->salary);
   a:
    printf("Enter Date of Joining\n");
    scanf("%d",&a->doj.day);
    if(a->doj.day<0||a->doj.day>31)
    {
        printf("Enter a valid number from 1 to 31\n");
        goto a;
    }
    printf("Enter Month of Joining\n");
    scanf("%s",&a->doj.month);
    
    printf("Enter Year of Joining\n");
    scanf("%d",&a->doj.year);
}

void output(struct employee a)
{
    printf("\nEmployee ID     : %d\n",a.employee_id);
    printf("Employee Name    : %s\n",a.name);
    printf("Employee Salary  : %f\n",a.salary);
    printf("Day of Joining   : %d\n",a.doj.day);
    printf("Month of Joining : %s\n",a.doj.month);
    printf("Year of Joining  : %d\n",a.doj.year);
}

int main()
{
    struct employee e;
    input(&e);
    output(e);
    return 0;
}
