#include<stdio.h>

void input(int *num)
{
    printf("Enter the number\n");
    scanf("%d",num);
}

int compute(int num)
{
    int rev=0,rem;
    while(num>0)
    {
        rem=num%10;
        rev=rev*10+rem;
        num=num/10;
    }
    return rev;
}

int compare(int num,int rev)
{
    if(num==rev)
        return 1;
    else
        return 0;
}

void output(int num,int rev,int a)
{
    if(a==1)
        printf("The number %d is a palindrome and its reverse is %d\n",num,rev);
    else
        printf("The number %d is not a palindrome and its reverse is %d\n",num,rev);
}

int main()
{
    int num,rev,a;
    input(&num);
    rev=compute(num);
    a=compare(num,rev);
    output(num,rev,a);
    return 0;
}