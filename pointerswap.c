
#include<stdio.h>

void input(float *a,float *b)
{
    printf("Enter the first number\n");
    scanf("%f",a);
    printf("Enter the second number\n");
    scanf("%f",b);
}

void compute(float *a,float *b)
{
    *a=*a+*b;
    *b=*a-*b;
    *a=*a-*b;
}

void output(float *a,float *b)
{
    printf("After interchanging, the First number is %f and the Second number is %f.\n",*a,*b);
}
int main()
{
    float a,b;
    input(&a,&b);
    compute(&a,&b);
    output(&a,&b);
    return 0;
}
