#include <stdio.h>

void inputfile()
{
    char a;
    FILE *fp;
    fp=fopen("input.txt","w");
    printf("Enter the Data and Press Ctrl D after entering the data.\n");
    while((a=getchar())!=EOF)
    {
        fputc(a,fp);
    }
    fclose(fp);
}

void outputfile()
{
    char a;
    FILE *fp;
    fp=fopen("input.txt","r");
    printf("Content of the file is :\n");
    while((a=fgetc(fp))!=EOF)
    {
        printf("%c",a);
    }
    printf("\n");
}

int main()
{
    inputfile();
    outputfile();
    return 0;
}
