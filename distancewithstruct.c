#include<stdio.h>
#include<math.h>

struct point
{
    int x;
    int y;
};

void input(struct point *a)
{
    printf("Enter X Coordinate\n");
    scanf("%d",&a->x);
    printf("Enter Y Coordinate\n");
    scanf("%d",&a->y);
}

float compute(struct point a,struct point b)
{
    float dist;
    dist=sqrt(pow(a.x-b.x,2)+(pow(a.y-b.y,2)));
    return dist;
}

void output(struct point a,struct point b,float e)
{
    printf("The Distance between the coordinates A(%d,%d) and B(%d,%d) is : %f\n",a.x,a.y,b.x,b.y,e);
}
int main()
{
    struct point p1,p2;
    float e;
    input(&p1);
    printf("\nEnter the Coordinates of the Second Point\n\n");
    input(&p2);
    e=compute(p1,p2);
    output(p1,p2,e);
    return 0;
}