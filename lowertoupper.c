#include<stdio.h>

void input(char str[50])
{
    printf("Enter the String\n");
    gets(str);
}

void compute(char str[50])
{
    int i;
    for(i=0;str[i]!='\0';i++)
    {
        if(str[i]>96&&str[i]<123)
            str[i]=str[i]-32;
    }
}

void output(char str[50])
{
    printf("The changed string is : \n");
    puts(str);
}

int main()
{
    char str[50];
    input(str);
    compute(str);
    output(str);
    return 0;
}
