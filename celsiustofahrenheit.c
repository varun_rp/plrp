#include <stdio.h>

void input(float *a)
{
    printf("Enter the Temperature in Fahrenheit \n");
    scanf("%f",a);
}

float compute(float a)
{
    float b,c,d;
    c=(a-32)*5;
    d=c/9;
    return d;
}

void output(float a,float b)
{
    printf("The Temperature of %f in fahrenheit is %f degree celsius\n",a,b);
}

int main()
{
    float a,b;
    input(&a);
    b=compute(a);
    output(a,b);
    return 0;
}
