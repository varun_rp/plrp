#include<stdio.h>

void input(int *num)
{
    printf("Enter the number\n");
    scanf("%d",num);
}

int compute(int num)
{
    int b,c,sum=0;
    while(num!=0)
    {
        b=num/10;
        c=num%10;
        sum=sum+c;
        num=b;
    }
    return sum;
}

void output(int num,int sum)
{
    printf("The sum of digits in %d is %d\n",num,sum);
}

int main()
{
    int num,sum;
    input(&num);
    sum=compute(num);
    output(num,sum);
    return 0;
}