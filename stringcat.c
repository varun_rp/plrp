#include<stdio.h>

void input(char *st)
{
    printf("Enter the string\n");
    gets(st);
}

char *mystrcat(char *st1,const char *st2)
{
    int i,j;
    for(i=0;st1[i]!='\0';i++);
    //st1=st1+i;
    for(j=0;st2[j]!='\0';j++)
    {
        st1[i]=st2[j];
        i++;
    }
    st1[i]='\0';
    return st1;
}

void output(char *st)
{
    printf("The concatenated string is :\n");
    puts(st);
}

int main()
{
    char s1[50],s2[50];
    input(s1);
    input(s2);
    char *con;
    con=mystrcat(s1,s2);
    output(con);
    return 0;
}