#include<stdio.h>

void input(int *num)
{
    printf("Enter the no of lines to print '*'\n");
    scanf("%d",num);
}

void output(int num)
{
    int i,j;
    printf("\n");
    for(i=0;i<num;i++)
    {
        for(j=0;j<=i;j++)
        {
            printf("* ");
        }
        printf("\n");
    }
}

int main()
{
    int num;
    input(&num);
    output(num);
    return 0;
}