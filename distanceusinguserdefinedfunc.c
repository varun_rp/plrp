#include<stdio.h>
#include<math.h>

void input(int *a,int *b,int *c,int *d)
{
    printf("Enter X1\n");
    scanf("%d",a);
    printf("Enter Y1\n");
    scanf("%d",b);
    printf("Enter X2\n");
    scanf("%d",c);
    printf("Enter Y2\n");
    scanf("%d",d);
}

float compute(int x1,int y1,int x2,int y2)
{
    float dist;
    dist=sqrt(pow(x2-x1,2)+(pow(y2-y1,2)));
    return dist;
}

void output(int a,int b,int c,int d,float e)
{
    printf("The Distance between the coordinates A(%d,%d) and B(%d,%d) is : %f\n",a,b,c,d,e);
}
int main()
{
    int a,b,c,d;
    float e;
    input(&a,&b,&c,&d);
    e=compute(a,b,c,d);
    output(a,b,c,d,e);
    return 0;
}