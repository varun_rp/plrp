#include<stdio.h>

void input(float *a,float *b)
{
    printf("Enter the First Number\n");
    scanf("%f",a);
    printf("Enter the Second Number\n");
    scanf("%f",b);
}

float compute(float a, float b,float c)
{
    c=a+b;
    return c;
}

void output(float a,float b,float c)
{
    printf("The sum of %f and %f is : %f\n",a,b,c);
}
int main()
{
    float a,b,c;
    input(&a,&b);
    c=compute(a,b,c);
    output(a,b,c);
    return 0;
}