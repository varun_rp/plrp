#include<stdio.h>

void input(float *a,float *b)
{
    printf("Enter the First Number\n");
    scanf("%f",a);
    printf("Enter the Second Number\n");
    scanf("%f",b);
}

void compute(float a, float b)
{
    (a>b) ? printf("The Greatest Number between %f and %f is %f\n",a,b,a) : printf("The Greatest Number between %f and %f is %f\n",a,b,b);
}

int main()
{
    float a,b;
    input(&a,&b);
    compute(a,b);
    return 0;
}