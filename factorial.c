#include<stdio.h>

void input(int *num)
{
    printf("Enter the Number\n");
    scanf("%d",num);
}

int compute(int num)
{
    int prod=1,i=1;
    if(num==0)
        return 1;
    for(i;i<=num;i++)
    {
        prod=prod*i;
    }
    return prod;
}

void output(int num,int prod)
{
    printf("The factorial of %d is %d\n",num,prod);
    int i=num;
    for(i;i>=2;i--)
        printf("%d * ",i);
    if(num!=0)
        printf("1 = %d\n",prod);
}

int main()
{
    int num,ans;
    input(&num);
    ans=compute(num);
    output(num,ans);
    return 0;
}
