#include<stdio.h>

void input(int *num,int *order)
{
    printf("Enter the Number\n");
    scanf("%d",num);
    printf("Enter the way it should be printed\n");
    printf("1.Horizontal\n");
    printf("2.Vertical\n");
    scanf("%d",order);
}

void output(int num,int order)
{
    int i=num-1;
    printf("\n%d",num);
    if(order==1)
    {
        for(i;i>=1;i--)
            printf(" %d",i);
        printf("\n");
    }
    else if(order==2)
    {
        for(i;i>=1;i--)
            printf("\n%d",i);
        printf("\n");
    }
    else
    {
        printf("\bExecute again\n");
    }
}

int main()
{
    int num,order;
    input(&num,&order);
    output(num,order);
    return 0;
}
