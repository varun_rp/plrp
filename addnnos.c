#include<stdio.h>

void input(float a[],int n)
{
    
    for(int i=0;i<n;i++)
    {
        printf("Enter the Number\n");
        scanf("%f",&a[i]);
    }
}

float compute(float a[],int n)
{
    float c=0;
    int i;
    for(i=0;i<n;i++)
        c=c+a[i];
    return c;
}

void output(float a)
{
    printf("The sum is : %f\n",a);
}
int main()
{
    float a[15],c;
    int n;
    printf("Enter the total number of Inputs\n");
    scanf("%d",&n);
    input(a,n);
    c=compute(a,n);
    output(c);
    return 0;
}