#include<stdio.h>

void input(int *num)
{
    printf("Enter the no of lines to print '*'\n");
    scanf("%d",num);
}

void output(int num)
{
    int i,j;
    printf("\n");
    for(i=65;i<65+num;i++)
    {
        for(j=65;j<=i;j++)
        {
            printf("%c ",j);
        }
        printf("\n");
    }
}

int main()
{
    int num;
    input(&num);
    output(num);
    return 0;
}
