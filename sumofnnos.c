#include<stdio.h>

void input(int *num)
{
    printf("Enter the no of natural numbers\n");
    scanf("%d",num);
}

int compute(int num)
{
    int i,b,c,sum=0;
    for(i=1;i<=num;i++)
    {
        sum=sum+i;
    }
    return sum;
}

void output(int num,int sum)
{
    printf("The sum of %d natural numbers is %d\n",num,sum);
}

int main()
{
    int num,sum;
    input(&num);
    sum=compute(num);
    output(num,sum);
    return 0;
}