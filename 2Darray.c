#include<stdio.h>

void input(int marks[5][3])
{
    int i,j;
    for(i=0;i<3;i++)
    {
        for(j=0;j<5;j++)
        {
            if(i==0)
            {
                printf("Enter the marks of student %d in PHYSICS\n",j+1);
                scanf("%d",&marks[j][i]);
            }
            else if(i==1)
            {
                printf("Enter the marks of student %d in CCP\n",j+1);
                scanf("%d",&marks[j][i]);
            }
            else
            {
                printf("Enter the marks of student %d in MATHS\n",j+1);
                scanf("%d",&marks[j][i]);
            }
        }
    }
}

int compute(int marks[5][3],int *phy,int *ccp)
{
    int i,j;
    int maths=marks[0][2],phys=marks[0][0],cc=marks[0][1];
    for(i=0;i<3;i++)
    {
        for(j=0;j<5;j++)
        {
            if(i==0)
            {
                 if(marks[j][i]>=phys)
                     phys=marks[j][i];
            }
            else if(i==1)
            {
               if(marks[j][i]>=cc)
                     cc=marks[j][i];
            }
            else
            {
                if(marks[j][i]>=maths)
                     maths=marks[j][i];
            }
        }
    }
    *phy=phys;
    *ccp=cc;
    return maths;
}

void output(int *phy,int *ccp, int math)
{
    printf("The highest marks in PHYSICS is : %d\n",*phy);
    printf("The highest marks in CCP     is : %d\n",*ccp);
    printf("The highest marks in MATH    is : %d\n",math);
}

int main()
{
    int marks[5][3];
    int phys,ccp,maths;
    input(marks[5]);
    maths=compute(marks[5],&phys,&ccp);
    output(&phys,&ccp,maths);
    return 0;
}
