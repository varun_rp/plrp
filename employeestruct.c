#include<stdio.h>
#include<math.h>

struct employee
{
    char name[50];
    int age;
    char gender[50];
};

int input(struct employee a[50])
{
    int i,n;
    printf("Enter the no of entries\n");
    scanf("%d",&n);
    for(i=0;i<n;i++)
    {
        printf("Enter the name\n");
        scanf("%s",&a[i].name);
        printf("Enter the age\n");
        scanf("%d",&a[i].age);
        printf("Enter the Gender\nMale or\nFemale or\nNo Gender\n");
        scanf("%s",&a[i].gender);
    }
    return n;
}

void output(struct employee a[50],int n)
{
    int i=0;
    for(i=0;i<n;i++)
    {

        printf("\nName : %s\n",a[i].name);
        printf("Age : %d\n",a[i].age);
        printf("Gender : %s",a[i].gender);
    }
}
int main()
{
    struct employee a[50];
    int n;
    n=input(a);
    output(a,n);
    return 0;
}
