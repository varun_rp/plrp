#include<stdio.h>

int input(int ar[50])
{
    int i,n;
    printf("Enter the number of entries\n");
    scanf("%d",&n);
    printf("Enter the Numbers\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&ar[i]);
    }
    return n;
}

int compute(int ar[50],int n)
{
    int i,smallnum=ar[0],largenum=ar[0];
    for(i=1;i<n;i++)
    {
        if(smallnum>=ar[i])
            smallnum=ar[i];
         if(largenum<=ar[i])
            largenum=ar[i];
    }

    for(i=0;i<n;i++)
    {
        if(ar[i]==smallnum)
            ar[i]=largenum;
        else if(ar[i]==largenum)
            ar[i]=smallnum;
    }
}

void output(int ar[50],int n)
{
    int i;
    printf("The interchanged array is :\n");
    for(i=0;i<n;i++)
    {
        printf("%d ",ar[i]);
    }
}

int main()
{
    int ar[50],n;
    n=input(ar);
    compute(ar,n);
    output(ar,n);
    return 0;
}
