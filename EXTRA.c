#include <stdio.h>
#include <string.h>
#include <ctype.h>

void nooflines(int *lines)
{
    printf("Enter the Number of lines of Code to be Encrypted\n");
    scanf("%d",lines);
}

void shift(int *factor)
{
    printf("Enter the Shift Factor\n");
    scanf("%d",factor);
}

void inputdata(char data[][50],int lines)
{
    int i;
    printf("Enter the Code in 'CAPITAL LETTERS'. Press Enter to input the next line.\n\n");
    for(i=0;i<lines;i++)
    {
        scanf("%s",&data[i][0]);
    }
}

void outputdata(char data[][50],int lines)
{
    int i;
    printf("\nThe encrypted code is :\n");
    for(i=0;i<lines;i++)
    {
        printf("%s\n",data[i]);
    }
}

void strrev(char data[50])
{
    char dup[50];
    int i=strlen(data);
    int j,k;
    for(k=0,j=i-1;j>=0;j--,k++)
    {
        dup[k]=data[j];
    }
    dup[k+1]='\0';
    strcpy(data,dup);
}

void computedata(char data[][50],int lines,int factor)
{
    int i,j,extra,a;
    char duplicate,dup[lines][50];
    for(i=0;i<lines;i++)
    {
        for(j=0;data[i][j]!='\0';j++)
        {
            if((ispunct(data[i][j]))==0||isspace(data[i][j])==0)
            {
                if(factor >= -25 && factor <= 25)
                {
                    duplicate=factor+data[i][j];
                    if(duplicate<65)
                    {
                        duplicate=26+duplicate;
                    }
                    else if(duplicate>90)
                    {
                        duplicate=duplicate-26;
                    }
                    data[i][j]=duplicate;
                }
            }
            else
            {
                data[i][j]=data[i][j];
            }
        }
        strrev(data[i]);
    }

}
int main()
{
    int lines,factor;
    nooflines(&lines);
    char data[lines][50];
    shift(&factor);
    inputdata(data,lines);
    computedata(data,lines,factor);
    outputdata(data,lines);
    return 0;
}
