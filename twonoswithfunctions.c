#include<stdio.h>

void input(float *a,float *b)
{
    printf("Enter the First Number\n");
    scanf("%f",a);
    printf("Enter the Second Number\n");
    scanf("%f",b);
}

float compute(float a, float b)
{
    if(a>b)
        return a;
    else
        return b;
}

void output(float a,float b,float c)
{
    printf("The Greatest Number between %f and %f is : %f\n",a,b,c);
}
int main()
{
    float a,b,c;
    input(&a,&b);
    c=compute(a,b);
    output(a,b,c);
    return 0;
}