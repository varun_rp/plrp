#include <stdio.h>
#include <math.h>
void input(int *a,int *b,int *c)
{
    printf("The Quadratic Equation is in the form : ax^2 + bx + c\n");
    
    printf("\nEnter the value of a\n");
    scanf("%d",a);
    printf("Enter the value of b\n");
    scanf("%d",b);
    printf("Enter the value of c\n");
    scanf("%d",c);
}

void compute(int a,int b,int d,float *x1,float *x2)
{
    if(d>=0)
    {
        *x1=(-b+sqrt(d))/2*a;
        *x2=(-b-sqrt(d))/2*a;
    }
    else
    {
        *x1=-b/2*a;
    }
}

void output(int a,int b,int c,float x1,float x2)
{
    printf("The roots of the given Equation %dx^2 + %dx + %d = 0 is %0.2f and %0.2f\n",a,b,c,x1,x2);
}

int main()
{
    int a,b,c,d;
    float x1,x2;
    input(&a,&b,&c);
    d=((b*b)-(4*a*c));
    compute(a,b,d,&x1,&x2);
    switch(d>0)
    {
        case 1: output(a,b,c,x1,x2); break;
    }
    switch(d<0)
    {
        case 1: printf("The equation %dx^2 + %dx +%d have imaginary roots which are \n%0.2f + %0.2fi and %0.2f - %0.2fi\n",a,b,c,x1,(sqrt(-d)/2*a),x1,(sqrt(-d)/2*a)); break;
    }
    switch(d==0)
    {
        case 1: printf("The roots are equal where the roots of the equation %dx^2 + %dx + %d are %0.2f\n",a,b,c,x1); break;
    }
}