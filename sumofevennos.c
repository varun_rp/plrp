#include<stdio.h>
#include<math.h>

void input(int *num)
{
    printf("Enter the no of even numbers\n");
    scanf("%d",num);
}

int compute(int num)
{
    int i,j=2,sum=0,sq;
    for(i=0;i<num;i++)
    {
        sq=pow(j,2);
        sum=sum+sq;
        j+=2;
    }
    return sum;
}

void output(int num,int sum)
{
    printf("The sum of %d Even numbers is %d\n",num,sum);
}

int main()
{
    int num,sum;
    input(&num);
    sum=compute(num);
    output(num,sum);
    return 0;
}